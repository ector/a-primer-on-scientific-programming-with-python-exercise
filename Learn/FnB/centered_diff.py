"""Chapter 3 exercise 18"""
from sympy import symbols, lambdify, ln, exp, cos, pi
#A
def diff(func, x_value, delta=1E-5):
    """Find an approximate derivative of a mathematical
    function f(x) if h is small """
    xdelta_plus = x_value + delta
    xdelta_minus = x_value - delta
    f_x = lambdify(X, func)
    return (f_x(xdelta_plus) - f_x(xdelta_minus)) / (2 * delta)
#B
def test_diff():
    """Test diff (approximate derivative)"""
    df_exact = 1
    df_approx = diff(cos(X), 0)
    tol = 1e3
    success = abs(df_exact - df_approx) < tol
    msg = 'approximate derivative expected tolerance is higher'
    assert success, msg
#C
def application():
    """applying diff"""
    h_int = 0.01
    f_x = [exp(X), exp(-2*X**2), cos(X), ln(X)]
    xvalue = [0, 0, (2 * pi), 1]
    for fx_i, xvalue_i in zip(f_x, xvalue):
        approx_error = abs(diff(fx_i, xvalue_i, h_int) - diff(fx_i, xvalue_i))
        print('Error in approximate derivate is %f ' % (approx_error))

X = symbols('x')
test_diff()
application()
