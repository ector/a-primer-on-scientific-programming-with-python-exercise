# -*- coding: utf-8 -*-
"""
Created on Mon Nov 24 18:32:09 2014

@author: ector
"""

print('-------------------------------')

def polyprod(x, roots):
    p_x = x
    for i in roots:
        p_x = p_x * (x - i)
    return p_x
    
def test_polyprod():
    a = 6
    roots = [3, 2, 6]
    px_x = polyprod(a, roots)
    tol = 1e-5
    success = abs(0 - px_x) < tol
    msg = 'polynomial roots is greater than 0 and should not be'
    assert success, msg

test_polyprod()
ROOTS = [5, 3, 8, 6, 3]
X = 8.4
Fx = polyprod(X, ROOTS)
print('polynomial of ', ROOTS, ' at x = %.2f is: %.2f' % (X, Fx))
print('-------------------------------')
