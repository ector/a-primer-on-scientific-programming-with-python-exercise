"""Chapter 3 exercise 20"""

def kinematics(x_distance, i_index, d_t=1E-6):
    """Computing velocity and acceleration at different point"""
    v_dx = x_distance[i_index + 1] - x_distance[i_index - 1]
    v_dt = d_t[i_index + 1] - d_t[i_index - 1]
    v_index = v_dx / v_dt
    a_dx1 = x_distance[i_index + 1] - x_distance[i_index]
    a_dx2 = x_distance[i_index] - x_distance[i_index - 1]
    a_dt1 = d_t[i_index + 1] - d_t[i_index]
    a_dt2 = d_t[i_index] - d_t[i_index - 1]
    a_index = (2 / v_dt) * ((a_dx1 / a_dt1) - (a_dx2 / a_dt2))
    return v_index, a_index

def test_kinematics():
    """testing kinematics"""
    t_list = [0, 0.5, 1.5, 2.2]
    v_const = 2
    x_list = [v_const * i for i in t_list]
    velocity, acceleration = kinematics(x_list, 1, t_list)
    real_acceleration = 0
    tol = 1e-2
    success = abs(real_acceleration - acceleration) < tol and abs(v_const - velocity) < tol
    msg = 'At constant velocity of %.2f m/s acceleration should be 0.00 but it was %.2f' \
        % (v_const, acceleration)
    assert success, msg

test_kinematics()
