# -*- coding: utf-8 -*-
"""
Created on Thu Nov 20 18:47:39 2014

@author: ector
"""


print('-------------------------------')
def my_sum(M):
    if isinstance(M[0], (int, float)):
        summ = 0
        for i in M:
            summ += i
    elif isinstance(M[0], (str)):
        summ = None
        summ = [''.join(i for i in M)]
    else:
        summ = []
        for i in M:
            summ = summ + i
    print(summ)
my_sum([1, 3, -5, 5])
my_sum([1.4, 3.3, -5, 5])
my_sum(['A', 'D', 'C', 'F', 'E'])
my_sum([['A', 'D'], ['C', 'F'], ['E']])
print('-------------------------------')
