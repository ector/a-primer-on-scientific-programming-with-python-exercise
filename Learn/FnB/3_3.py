# -*- coding: utf-8 -*-
"""
Created on Wed Nov 19 20:16:16 2014

@author: ector
"""

print('-------------------------------')
from numpy.lib.scimath import sqrt

def ROOTS(a, b, c):
    d = sqrt(b**2 - 4*a*c)
    r1 = (-b + d) / (2.0 * a)
    r2 = (-b - d) / (2.0 * a)
    return r1, r2
    
def test_roots():
    a = 1
    b = 4
    c = 1
    r1, r2 = ROOTS(a, b, c)
    tol = 1e-5
    exact_r1 = -0.26794 #calculated root by hand
    exact_r2 = -3.73205 #calculated root by hand
    success = abs(exact_r1 - r1) < tol and abs(exact_r2 - r2) < tol \
    and isinstance(r1, float)
    msg = 'Roots failed: one of the two roots greater than the exact value'
    assert success, msg

def test_roots_complex():
    a = 1
    b = 2
    c = 100
    r1, r2 = ROOTS(a, b, c)
    tol = 1e-5
    exact_r1 = -1+9.94987j #calculated root by hand
    exact_r2 = -1-9.94987j #calculated root by hand
    success = abs(exact_r1 - r1) < tol and abs(exact_r2 - r2) < tol \
    and isinstance(r1, complex)
    msg = 'Roots failed: one of the two roots greater than the exact value'
    assert success, msg

test_roots()
test_roots_complex()

x1, x2 = ROOTS(2, 4, 1)
print('roots are ', x1, 'and', x2)
print('-------------------------------')
