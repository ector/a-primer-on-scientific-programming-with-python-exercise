# -*- coding: utf-8 -*-
"""
Created on Wed Dec  3 20:23:45 2014

@author: ector
"""

print('-------------------------------')
from math import sqrt as sqt

def pathlength(x, y):
    if len(x) != len(y):
        return 'Array x and y are not of equal length'
    add = 0
    for i in range(1, len(x)):
        add += sqt((x[i] - x[i - 1])**2 + (y[i] - y[i - 1])**2)
    return add   
      
def test_pathlength():
    a = [1, 2, 3]
    b = [2, 3, 4]
    exact = 2.82842 #to 5 decimal places
    length = pathlength(a, b)
    tol = 1e-5
    success = abs(exact - length) < tol
    msg = 'Length of a path should be %.2f but was %.2f' % (exact, length)
    assert success, msg

X = range(2, 100, 5)
Y = range(4, 200, 10)
Y_WL = range(1, 100, 10) # Y with wrong number of index
test_pathlength()
try:
    print('Length of a random path is = %.2f' % (pathlength(X, Y)))
except:
    print('%s' % (pathlength(X, Y)))
##############################################################
#This shous what happens when arrays are not of the same size#
##############################################################
#try:
#    print('Length of a random path is = %.2f' % (pathlength(X, Y_WL)))
#except:
#    print('%s' % (pathlength(X, Y_WL)))
print('-------------------------------')
