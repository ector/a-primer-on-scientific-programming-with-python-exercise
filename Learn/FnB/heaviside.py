"""Chapter 3 exercise 3.23"""
#A
def heaviside(value):
    """Heaviside function"""
    if value < 0:
        return 0
    else:
        return 1
#B
def test_heaviside():
    """testing heaviside"""
    tol = 0
    val = [-10, -10**-15, 0, 10**-15, 10]
    for i in val:
        if i < 0:
            success = abs(heaviside(i) - 0) == tol
        else:
            success = abs(heaviside(i) - 1) == tol
    msg = 'Heaviside function is wrong'
    assert success, msg

test_heaviside()
