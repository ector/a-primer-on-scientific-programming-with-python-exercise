# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 19:41:18 2015

@author: Adetola
"""

from Learn.FnB.indicator_func import indicator_one

DATA = [(0, -1), (1, 2), (1.5, 5), (2, 10), (3, 20)]

def piecewise_two(x_sample, data):
    """Chapter 3 exercise 3.27, function piecewise(x, data) for evaluating
    piecewise mathematical function at point x"""
    fx_sum = 0
    for i in range(len(data) - 1):
        fx_sum = fx_sum + (data[i][0] * indicator_one(x_sample, data[i][1], data[i+1][1]))
    return fx_sum

piecewise_two(3, DATA)
