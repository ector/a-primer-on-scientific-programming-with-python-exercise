# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 18:15:51 2015

@author: Adetola
"""

DATA = [(0, -1), (1, 0), (1.5, 4)]

def piecewise(x_sample, data):
    """Chapter 3 exercise 3.26, function piecewise(x, data) for evaluating
    piecewise mathematical function at point x"""
    for i in range(len(data) - 1):
        if x_sample in range(data[i][1], data[i+1][1]):
            return data[i][0]

def test_piecewise():
    """test piecewise constant"""
    x_sam = 12
    test_data = [(0, -1), (1, 2), (1.5, 5), (2, 10), (3, 20)]
    actual_result = piecewise(x_sam, test_data)
    real_result = 2
    tol = 1e-2
    success = abs(actual_result - real_result) < tol
    msg = 'Piecewise constant value is %s and should be %s' %(actual_result, real_result)
    assert success, msg

test_piecewise()
