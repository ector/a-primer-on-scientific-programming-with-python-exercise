# -*- coding: utf-8 -*-
"""
Created on Wed Jan 14 20:06:40 2015

@author: ector
"""

print('-------------------------------')
from math import sin, pi

## Answer A
def S(t, n, T):
    S_t_n = 0    
    f_pi = 4 / pi
    for i in range(1, n):
        x = (2 * ((2 * i) - 1) * pi * t) / T
        k = 1 / ((2 * i) - 1)
        S_t_n += k * sin(x)
    S_t_n = f_pi * S_t_n
    return S_t_n

## Answer B
def F(t, T):
    if 0 < t < (T/2):
        f_t = 1
    elif (T/2) < t < T:
        f_t = -1
    else:
        f_t = 0
    return f_t

## Answer C
def sinesum1():
    n = [1, 3, 5, 10, 30, 100]
    alpha = [0.01, 0.25, 0.49]
    T = 2 * pi
    t = [T * i for i in alpha]
    print('n \t t \t error')
    for n_i in n:
        for t_i in t:
            err = F(t_i, T) - S(t_i, n_i, T)
            print('%.1f \t %.3f \t %.3f' % (n_i, t_i, err))
        print('\n')

sinesum1()        
print('-------------------------------')

