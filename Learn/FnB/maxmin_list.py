"""Chapter 3 exercise 3.22"""
def mymax(int_array):
    """Returns largest element in an array"""
    max_elem = int_array[0]
    for i in int_array[1:]:
        if i > max_elem:
            max_elem = i
    return max_elem

def mymin(int_array):
    """Returns smallest element in an array"""
    min_elem = int_array[0]
    for i in int_array[1:]:
        if i < min_elem:
            min_elem = i
    return min_elem

X = [1, 3, 7, 5, 2]
print("Min and Max of array " + str(X) + " are %.2f and %.2f respectively" % (mymin(X), mymax(X)))
