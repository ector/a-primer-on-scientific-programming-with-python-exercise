# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 07:23:42 2015
Chapter 3 exercise 3.25
@author: Adetola
"""

from Learn.FnB.heaviside import heaviside as H

def indicator_one(x_sample, l_boundary, r_boundary):
    """Check if x an element of an array"""
    if x_sample in range(l_boundary, r_boundary):
        return 1
    else:
        return 0

def indicator_two(x_sample, l_boundary, r_boundary):
    """Using heaviside to indicate"""
    indicator = H(x_sample - l_boundary) * H(x_sample - r_boundary)
    return indicator

def test_indicator():
    """test the two indicators"""
    x_sam = 10
    l_bound = 3
    r_bound = 9
    msg = 'One of the two indicator functions is wrong'
    success = indicator_one(x_sam, l_bound, r_bound) == indicator_one(x_sam, l_bound, r_bound)
    assert success, msg

test_indicator()
