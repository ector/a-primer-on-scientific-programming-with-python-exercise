"""Chapter 3 exercise 19"""
def fact(integer):
    """Factorial of a number"""
    sumf = 1
    if integer in [0, 1]:
        return sumf
    else:
        for i in range(1, integer + 1):
            sumf = sumf * i
        return sumf
INT = 5
print('factorial of int %d is %f ' % (INT, fact(INT)))
