"""Chapter 3 exercise 3.21"""
from sympy import symbols, lambdify, cos, pi
def maxmin(func, l_value, u_value, intervals=100):
    """Compute maximum and minimum values of a mathematical function
    f(x) on [a_value,u_value] with large number (dn) between a_value
     and u_value"""
    d_intervals = (u_value - l_value) / (intervals - 1)
    y_list = []
    for i in range(0, intervals):
        x_i = l_value + (i * d_intervals)
        f_x = lambdify(X, func)
        y_list.append(f_x(x_i))
    return max(y_list), min(y_list)

def test_maxmin():
    """testing maxmin"""
    exact_maximum, exact_minimum = 1, -1
    maximum, minimum = maxmin(cos(X), (-pi / 2), (2 * pi))
    tol = 1e-3
    success = abs(exact_maximum - maximum) < tol and abs(exact_minimum - minimum) < tol
    msg = 'wrong maximum and minimum value of f(x) = cos x, x (-pi/2, 2*pi) \
    are %.2f, %.2f respectively' % (maximum, minimum)
    assert success, msg

X = symbols('x')
test_maxmin()
