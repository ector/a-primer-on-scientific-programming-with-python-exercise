# -*- coding: utf-8 -*-
"""
Created on Wed Nov 19 19:31:16 2014

@author: ector
"""

print('-------------------------------')
def SUM_1K(M):
    s = 0
    for k in range(1, M + 1):
        s += 1.0/k
    return s
    
def test_SUM_1K():
    m = 3
    exact = 1.83333
    tol = 1e-5
    success = abs(exact - SUM_1K(m)) < tol
    msg = 'Sum failed: answer was greater than the exact value'
    assert success, msg
    
M = 10
test_SUM_1K()
print(SUM_1K(M))
print('-------------------------------')