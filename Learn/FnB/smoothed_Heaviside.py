"""Chapter 3 exercise 3.24"""
from math import sin, pi
#A
def heaviside_eps(value, eps=0.01):
    """Smooth heaviside function"""
    if value < -eps:
        h_x = 0
    elif value > eps:
        h_x = 1
    else:
        h_x = 0.5 + (value / (2 * eps)) + ((1 / (2 * pi)) * sin((pi * value)/eps))
    return h_x
#B
##write a test similar to the test in heaviside.py
