# -*- coding: utf-8 -*-
"""
Created on Thu Jan 15 19:51:59 2015

@author: ector
"""

print('-------------------------------')
from math import sqrt as sqt, pi, exp

def gauss(x, m=0, s=1):
    f_x = []
    for i in x:
        x_i = []
        k = -0.5 * (((i - m) / s)**2)
        c = 1/sqt(2 * pi * s)
        f_x.append(c * exp(k))
    return f_x

def gaussian(n, m, s):
    print('x \t f(x)')
    for i in range(1, int(n)):
        m *= i
        s *= i
        n_x = [m - (5 * s), m + (5 * s)]
        for x_i, fx_i in zip(n_x, gauss(n_x, m, s)):
            print('%.0f \t %.1g' % (x_i, fx_i))
        
N = 5.0
M = 3.0
S = 8.0
gaussian(N, M, S)
print('-------------------------------')