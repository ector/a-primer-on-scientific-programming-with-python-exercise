# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 20:37:26 2015

@author: Adetola
"""

def count_pairs(dna, pair):
    """Chapter 3 exercise 3.35, function count_pairs(dna, pair) return the 
    number of occurrences a pair of characters (pair) in a DNA string (dna)"""
    occurrences = 0
    for i in range(len(dna) - 1):
        jointpair = dna[i] + dna[i+1]
        if jointpair == pair:
            occurrences += 1
    return occurrences

DNA = 'ACTGCTATCCATT'
PAIR = 'AT'    
print(count_pairs(DNA, PAIR))