# -*- coding: utf-8 -*-
"""
Created on Thu Nov 27 20:05:45 2014

@author: ector
"""

print('-------------------------------')
from math import sin, cos, pi


x_min = 0
x_max = pi/2
dl = 10

# answer D
def midpointint(f, a, b, n):
    dx = (b - a) / n
    area = 0.0
    for i in range(1, n):
        area += f(a + (i * dx) + (0.5 * dx))
    area *= dx
    return area

def test_midpointint():
    a = 10
    exact = 1
    area = midpointint(cos, 0, pi/2, a)
    tol = 1e2
    success = abs(exact - area) < tol
    msg = 'midpoint calculation might be wrong'
    assert success, msg

test_midpointint()
# answer B
FXX = midpointint(sin, x_min, x_max, dl)
print('Midpoint integration rule answer is %f ' % (FXX))
print('-------------------------------')
