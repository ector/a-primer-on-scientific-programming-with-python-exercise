# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 20:50:58 2015

@author: Adetola
"""

def count_substr(dna, substr):
    """Chapter 3 exercise 3.34, function count_pairs(dna, pair) return the
    number of occurrences a pair of characters (pair) in a DNA string (dna)"""
    occurrences = 0
    for i in range(len(dna) - 1):
        jointsubstr = dna[i:i+3]
        if jointsubstr == substr:
            occurrences += 1
    return occurrences


DNA = 'ACGTTACGGAACG'
SUBSTR = 'ACG'
print(count_substr(DNA, SUBSTR))