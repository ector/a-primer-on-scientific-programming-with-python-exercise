# -*- coding: utf-8 -*-
"""
Created on Wed Dec  3 18:42:19 2014

@author: ector
"""

print('-------------------------------')
v1 = (-15, 11)
v2 = (28, 28)
v3 = (61, -13)
VERTICES = [v1, v2, v3]   
def area_triangle(vts):
    rowCounter = 1
    for i in range(len(vts)):
        xy = vts[i]
        if rowCounter == 1:
            x1 = xy[0]
            y1 = xy[1]
            rowCounter += 1
        elif rowCounter == 2:
            x2 = xy[0]
            y2 = xy[1]
            rowCounter += 1
        else:
            x3 = xy[0]
            y3 = xy[1]
    initial = abs((x2 * y3) - (x3 * y2) - (x1 * y3) + (x3 * y1) + (x1 * y2) \
    - (x2 * y1))
    return 0.5 * initial
      
def test_area_triangle():
    exact = 222.5
    area = area_triangle([[15, 15], [23, 30], [50, 25]])
    tol = 1e-2
    success = abs(exact - area) < tol
    msg = 'Area of triangle should be %.2f but was %.2f' % (exact, area)
    assert success, msg

test_area_triangle()
#Area = 
print('Area of arbitrary triangle ', VERTICES, ' is = %.2f' % (area_triangle(VERTICES)))

print('-------------------------------')
