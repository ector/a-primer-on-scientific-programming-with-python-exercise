"""
Created on Tue Dec  2 21:00:03 2014

@author: ector
"""

###################################################
###                                             ###
###         DO          NOT         COPY        ###
###                                             ###
###################################################
print('-------------------------------')
from math import sin, cos, pi


X_MIN = 0
X_MAX = pi/2
DL = 1E-5

# answer D
def trapezint3(f, a, b, dl):
    eps = dl
    dx = (b - a) / eps
    fa = f(a)
    fb = f(b)
    area1 = 0.5 * dx * (fa + fb)
    area = 0.0
    for i in range(1, int(dx)):
        area = area + (dx*f(a + (i * dx)))
    area = area1 + area
    return area

def test_midpointint():
    a = 10
    exact = 1
    area = trapezint3(cos, 0, pi/2, a)
    tol = 1e2
    success = abs(exact - area) < tol
    print(exact - area)
    msg = 'Trpazeint3 calculation might be wrong'
    assert success, msg

test_midpointint()
# answer B
FXX = trapezint3(sin, X_MIN, X_MAX, DL)
print('Trpazeint3 integration rule answer is %f ' % (FXX))
print('-------------------------------')
