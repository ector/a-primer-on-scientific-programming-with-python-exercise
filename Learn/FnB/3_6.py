# -*- coding: utf-8 -*-
"""
Created on Wed Nov 26 20:09:04 2014

@author: ector
"""

print('-------------------------------')
from math import sin, cos, pi


x_min = 0
x_max = pi/2
dl = 10
    
# answer A and B    
def trapezint1(f, a, b):
    fa = f(a)
    fb = f(b)
    area = 0.5 * (b - a) * (fa + fb)
    return area

# answer C
def trapezint2(f, a, b):
    dx = (b - a) / 2
    fa = f(a)
    fdx = f(dx)
    fb = f(b)
    area1 = 0.5 * ((a+dx) - a) * (fa + fdx)
    area2 = 0.5 * (b - (a+dx)) * (fdx + fb)
    area = area1 + area2
    return area

# answer D    
def trapezint3(f, a, b, n):
    dx = (b - a) / n
    fa = f(a)
    fb = f(b)
    area1 = 0.5 * dx * (fa + fb)
    area = 0.0
    for i in range(1, n):
        area = area + (dx*f(a + (i * dx)))
    area = area1 + area
    return area
    
def test_trapezintd():
    a = 10
    exact = 1
    area = trapezint3(cos, 0, pi/2, a)
    tol = 1e-1
    success = abs(exact - area) < tol
    msg = 'Trpazeint3 calculation might be wrong'
    assert success, msg

test_trapezintd()
# answer B
F = trapezint1(sin, x_min, x_max)
# answer C
Fx = trapezint2(sin, x_min, x_max)
# answer D
Fxx = trapezint3(sin, x_min, x_max, dl)
print('Area of  of a trapezodials in question B, C and D are %f , %f and %f'\
' respectively' % (F, Fx, Fxx))

print('-------------------------------')
