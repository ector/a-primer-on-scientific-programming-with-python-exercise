# -*- coding: utf-8 -*-
"""
Created on Thu Jan 15 20:03:18 2015

@author: ector
"""

print('-------------------------------')
import math as ma

M_O = [47, 67] # Egg mass in grams
K = 273.15
#T_y = 20 + k # York temperature in Kelvin
T_O = [4 + K, 25 + K] # Egg initial temperature

def egg(mass, To=20, Ty=70):
    rho = 1.038 # desity
    c = 3.7 # specific heat capacity J g^-1 K^-1
    tk = 0.0054 # thermal conductivity W cm^-1 K^-1
    tw = 100 + K # Water temperature in Kelvin
    t = ((mass**(2 / 3) * c * rho**(1 / 3)) / (tk * ma.pi**2 * (4*ma.pi/3)**(2 \
    / 3))) * (ma.log1p((0.76 * ((To - tw)/(Ty - tw)))))
    return t
    
def egg_func(mass, To):
    for m in mass:
        for to in To:
            print("""Time taken for %.2f grams of egg to boil from %.2f is: %g second(s)""" % (m, to-K, int(egg(m, to))))

egg_func(M_O, T_O)
print('-------------------------------')


