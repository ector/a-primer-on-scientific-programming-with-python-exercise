# -*- coding: utf-8 -*-
"""
Created on Wed Dec  3 22:55:41 2014

@author: ector
"""

print('-------------------------------')
from math import sqrt as sqt, cos, sin, pi

def pathlength(x, y):
    if len(x) != len(y):
        return 'Array x and y are not of equal length'
    add = 0
    for i in range(1, len(x)):
        add += sqt((x[i] - x[i - 1])**2 + (y[i] - y[i - 1])**2)
    return add
      
def test_pathlength():
    a = [1, 2, 3]
    b = [2, 3, 4]
    exact = 2.82842 #to 5 decimal places
    length = pathlength(a, b)
    tol = 1e-5
    success = abs(exact - length) < tol
    msg = 'Length of a path should be %.2f but was %.2f' % (exact, length)
    assert success, msg

def pi_approx():
    for k in range(2, 11):
        X, Y = [], []
        n = 2**k
        for i in range(n):
            X.append(0.5 * cos((2 * pi * i) / n))
            Y.append(0.5 * sin((2 * pi * i) / n))
        try:
            print('Error in the approximation of pi = %.2f' % (abs(pathlength(X, Y) - pi)))
        except:
            print('%s' % (pathlength(X, Y)))

test_pathlength()
pi_approx()
print('-------------------------------')
