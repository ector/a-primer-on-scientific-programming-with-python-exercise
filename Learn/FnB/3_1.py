# -*- coding: utf-8 -*-
"""
Created on Wed Nov 19 19:10:50 2014

@author: ector
"""

print('-------------------------------')
def F(C):
    return (9.0/5) * C + 32
    
def C(F):
    return (5.0/9) * (F - 32)
    

def F2C():
    for f in range(0, 100, 10):
        print('%.2f F is %.2f C degress' %(f, C(f)))

def test_C():
    c = 10
    tol = 1e-8
    success = abs(c - C(F(c))) < tol
    msg = 'Conversion failed: c after conversion \
    was greater than the initial value'
    assert success, msg
    
test_C()
F2C()
print('-------------------------------')