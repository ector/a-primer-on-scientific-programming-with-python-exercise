# -*- coding: utf-8 -*-
"""
Created on Tue Nov 18 22:34:16 2014

@author: ector
"""

print('-------------------------------')
C = 0
dC = 10
print('F C C-approximate')
C_arr = []; F_arr = []; Capp_arr = []
while C <= 40:
    C_arr.append(C)
    F = (9.0 / 5) * C + 32
    F_arr.append(F)
    Capp = (F - 30) / 2
    Capp_arr.append(Capp)
    #print(C, F, Capp)
    C += dC
all_arr = [F_arr, C_arr, Capp_arr]
for i in range(len(F_arr) - 1):
    print('\t')
    for j in all_arr:
        print('%d \t' % j[i], end=" ")
print('\n-------------------------------')
