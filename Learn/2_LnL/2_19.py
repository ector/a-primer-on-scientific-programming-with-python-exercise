# -*- coding: utf-8 -*-
"""
Created on Tue Nov 18 21:35:10 2014

@author: ector
"""

eps = 1.0 #initialize eps as 1e^-16
while 1.0 != 1.0 + eps: # loop to check that condition
    print('..........................', eps) # print the value of eps
    eps = eps/2.0 # divide eps by 2 and set it to eps
print('final eps:', eps) # print final eps
