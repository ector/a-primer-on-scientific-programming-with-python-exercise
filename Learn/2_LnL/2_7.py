# -*- coding: utf-8 -*-
"""
Created on Sun Nov 16 22:07:26 2014

@author: ector
"""

vo = 5
g = 9.81  # gravity
end = 2 * (vo / g) # upper limit
interval = 11
step = end/interval
tf = 0.0
    
print('t \t\t y(t)')
for t in range(interval):
    y_t = (vo * tf) - (0.5 * g * tf**2)
    print('%f \t %f' % (tf, y_t)) # print time and acceleration
    tf += step

tw = 0.0
print("t \t\t y(t)")
while tw < end:
    y = (vo * tw) - (0.5 * g * tw**2)
    print("%f \t %f" % (tw, y)) # print time and acceleration
    tw += step 
