# -*- coding: utf-8 -*-
"""
Created on Mon Nov 17 00:16:26 2014

@author: ector
"""

q = [['a', 'b', 'c'], ['d', 'e', 'f'], ['g', 'h']]

print(q[0][0])
print(q[1])
print(q[2][1])
print(q[1][0])
print(q[-1][-2]) # means last object in the array, and the next/second to 
#last element in the object 

for i in q:
    for j in range(len(i)):
        print(i[j])
print(type(j))
print(type(i))
""" i is a list object 
and j is an int object"""
        