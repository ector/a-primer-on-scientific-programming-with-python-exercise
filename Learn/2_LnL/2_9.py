# -*- coding: utf-8 -*-
"""
Created on Sun Nov 16 22:46:47 2014

@author: ector
"""

a = [1, 3, 5, 7, 11]
b = [13, 17]
c = a + b
print(c) # print out a list containing a and b respectively
b[0] = -1 # set first element in b to 0
d = [e+1 for e in a] # make a new array
print(d) # print out a list of a increase each element by 1
d.append(b[0] + 1) # append first element plus 1
d.append(b[-1] + 1) # append last element plus 1
print(d[-2:]) # print last two elements in d
for e1 in a:
    for e2 in b:
        print(e1 + e2) # add each element in b to a
