# -*- coding: utf-8 -*-
"""
Created on Mon Nov 17 22:18:10 2014

@author: ector
"""

vo = 5
g = 9.81  # gravity
end = 2 * (vo / g) # upper limit
interval = 11
step = end/interval
tf = 0.0
t_array = []
y_array = []
for i in range(interval):
    t_array.append(tf)
    tf += step

for t in t_array:
    y_t = (vo * t) - (0.5 * g * t**2)
    y_array.append(y_t)
    

print('t \t\t y(t)')
for t_element, y_element in zip(t_array, y_array):
    print('%f \t %f' % (t_element, y_element)) # print time and acceleration

print('\n')
ty1 = [t_array, y_array]
for i in range(len(t_array)):
    print('\t')
    for ty in ty1:
        print('%.2f\t' % ty[i], end=" ")
print('\n')
ty2 = [[T, Y] for T, Y in zip(t_array, y_array)]
# we print the elements, we know that t is index 0 and y(t) is index 1 in each of our list elements
for TY2 in ty2:
    print('%.2f\t%.2f' % (TY2[0], TY2[1]))
