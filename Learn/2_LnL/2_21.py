# -*- coding: utf-8 -*-
"""
Created on Tue Nov 18 21:53:10 2014

@author: ector
"""

import time
to = time.time()
while time.time() - to < 10:
    print('....I like while loops!')
    time.sleep(2)
print('Oh, no - the loop is over.')
# loop executed five time from 0 to 8

# dont run both while loops together
while time.time() - to > 10:
    print('....I like while loops!')
    time.sleep(2)
print('Oh, no - the loop is over.')
# loop wont execute because the loop condition is not met
