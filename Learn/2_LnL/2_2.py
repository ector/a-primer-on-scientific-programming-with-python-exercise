# -*- coding: utf-8 -*-
"""
Created on Sun Nov 16 11:06:00 2014

@author: ector
"""

print('-------------------------------')
C = 0
dC = 10
print('C F C-approximate')
while C <= 40:
    F = (9.0 / 5) * C + 32
    Capp = (F - 30) / 2
    print(C, F, Capp)
    C += dC
print('-------------------------------')