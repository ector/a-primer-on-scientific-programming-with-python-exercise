# -*- coding: utf-8 -*-
"""
Created on Tue Nov 18 21:44:26 2014

@author: ector
"""

a = 1/946.0*947
b = 1
if a != b: # Dont compare float because of round-off errors
    print('Wrong result')
    
#Better comparison
tol = 0.002 # set error tolerance to 0.002
if abs(a - b) < tol:
    print('Wrong result')    
