# -*- coding: utf-8 -*-
"""
Created on Sun Nov 16 22:19:54 2014

@author: ector
"""

vo = 5
g = 9.81  # gravity
end = 2 * (vo / g) # upper limit
interval = 11
step = end/interval
tf = 0.0
time_array = []
y_array = []
for i in range(interval):
    time_array.append(tf)
    tf += step

for t in time_array:
    y_t = (vo * t) - (0.5 * g * t**2)
    y_array.append(y_t)
    

print('t \t\t y(t)')
for t_element, y_element in zip(time_array, y_array):
    print('%f \t %f' % (t_element, y_element)) # print time and acceleration