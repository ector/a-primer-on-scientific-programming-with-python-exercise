"""
Created on Wed Nov 12 18:36:40 2014

@author: ector
"""

print('-------------------------------')
import math as ms

X = 1
M = 0
S = 2
Y = (1 / (ms.sqrt(2 * ms.pi) * S)) * ms.exp(-0.5 * ((X - M) / S)**2)
print('Gaussian function : %g' % Y)
print('-------------------------------')
