# -*- coding: utf-8 -*-
"""
Created on Tue Nov 11 23:14:18 2014

@author: ector
"""

A = 1000
p = 5
n = 3
A = A*(1+(p/100))**n
print('Account balance after %d years of interest at %.2f%% is %.2f euros' %(n, p, A))