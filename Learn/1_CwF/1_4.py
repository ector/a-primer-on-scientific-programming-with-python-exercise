# -*- coding: utf-8 -*-
"""
Created on Tue Nov 11 20:48:23 2014

@author: ector
"""

meter = 640
inches = meter / 0.0254
feet = inches / 12
yards = feet / 3
mile = yards / 1760
print("""meter=%.2f
inches=%.2f
feet=%.2f
yards=%.2f
mile=%f
""" % (meter, inches, feet, yards, mile))
