# -*- coding: utf-8 -*-
"""
Created on Tue Nov 11 18:36:17 2014

@author: ector
"""

v0 = 5
g = 9.81
t = 0.6
y = v0*t - 0.5*g*t**2
print("""At t=%f s, a ball with
initial velocity v0=%.3E m/s
is located at the height %.2f m.""" % (t, v0, y))
