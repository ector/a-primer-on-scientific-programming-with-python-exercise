# -*- coding: utf-8 -*-
"""
Created on Tue Nov 11 22:41:12 2014

@author: ector
"""

air = 0.0012
print('air of one litre mass is :%1.2f' %(air))
gasoline = 0.67
print('gasoline of one litre mass is :%1.2f kg' %(gasoline))
ice = 0.9
print('ice of one litre mass is :%1.2g kg' %(ice))
pure_water = 1.0
print('pure water of one litre mass is :%1.2g kg' %(pure_water))
seawater = 1.025
print('seawater of one litre mass is :%1.2g kg' %(seawater))
human_body = 1.03
print('human body of one litre mass is :%1.2g kg' %(human_body))
limestone = 2.6
print('limestone of one litre mass is :%1.2g kg' %(limestone))
granite = 2.7
print('granite of one litre mass is :%1.2g kg' %(granite))
iron = 7.8
print('iron of one litre mass is :%1.2g kg' %(iron))
silver = 10.5
print('silver of one litre mass is :%1.2g kg' %(silver))
mercury = 13.6
print('mercury of one litre mass is :%1.2g kg' %(mercury))
gold = 18.9
print('gold of one litre mass is :%1.2g kg' %(gold))
platinium = 21.4
print('platinium of one litre mass is :%1.2g kg' %(platinium))
Earth_mean = 5.52
print('Earth mean of one litre mass is :%1.2g kg' %(Earth_mean))
Earth_core = 13
print('Earth core of one litre mass is :%1.2g kg' %(Earth_core))
Moon = 3.3
print('Moon of one litre mass is :%1.2g kg' %(Moon))
Sun_mean = 1.4
print('Sun mean of one litre mass is :%1.2g kg' %(Sun_mean))
Sun_core = 160
print('Sun core of one litre mass is :%1.2g kg' %(Sun_core))
proton = 2.3E+14
print('proton of one litre mass is :%1.2g kg' %(proton))
