"""
Created on Wed Nov 12 20:17:57 2014

@author: ector
"""
import math as ma

M = 47 # Egg mass in grams
rho = 1.038 # desity
c = 3.7 # specific heat capacity J g^-1 K^-1
K = 0.0054 # thermal conductivity W cm^-1 K^-1
k = 273.15
Tw = 100 + k # Water temperature in Kelvin
Ty = 20 + k # York temperature in Kelvin
To = 4 + k # Egg initial temperature

t = ((M**(2 / 3) * c * rho**(1 / 3)) / (K * ma.pi**2 * (4*ma.pi/3)**(2 / 3))) \
* (ma.log1p((0.76 * ((To - Tw)/(Ty - Tw)))))

print("""Time taken for %.2f grams \n of egg to boil from %.2f is: %g second""" % (M, To-k, int(t)))
