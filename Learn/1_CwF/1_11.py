"""
Created on Wed Nov 12 18:49:53 2014

@author: ector
"""
import math as ma
C_D = 0.2 # Drag coefficient
A = 0.11 # Radius of the ball in meters
A_ = ma.pi * A**2
RHO = 1.2 # Density in kg/m^3
G = 9.81 # Gravity ms^-2
M = 0.43 # Mass in kg
V = 120 / 3.6 # Velocity, we divided by 3.6 to get m/s
F_G = M * G
F_D = 0.5 * C_D * RHO * A_ * V**2
RATIO = F_D/F_G               # drag force/gravity force
print("""Drag force at %g ms^-2 is %.2f Kgms^-2
Gravitational force is %.2f Kgms^-2
Ratio drag force/gravity force: %.2f""" % (V, F_D, F_G, RATIO))
