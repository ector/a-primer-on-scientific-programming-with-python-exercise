# -*- coding: utf-8 -*-
"""
Created on Tue Nov 11 20:33:02 2014

@author: ector
"""

max_year = 100
days_per_year =365
hours_per_day = 24
mins_per_hour = 60
secs_per_min = 60

baby_live_exp = max_year * days_per_year * hours_per_day * mins_per_hour * secs_per_min
print('years in secs=%.3g' % baby_live_exp)