# -*- coding: utf-8 -*-
"""
Created on Tue Nov 11 19:26:58 2014

@author: ector
"""

v0 = 5
g = 9.81
yc = 0.2
import math as m
t1 = (v0 - m.sqrt(v0**2 - 2*g*yc))/g
t2 = (v0 + m.sqrt(v0**2 - 2*g*yc))/g
print('At t=%g s and %g s, the height is %g m' % (t1, t2, yc))