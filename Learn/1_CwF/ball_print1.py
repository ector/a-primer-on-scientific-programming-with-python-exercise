# -*- coding: utf-8 -*-
"""
Created on Tue Nov 11 18:28:06 2014

@author: ector
"""

v0 = 5
g = 9.81
t = 0.6
y = v0*t - 0.5*g*t**2
print('At t=%g s, the height of the ball is %.2f m.' % (t, y))
